package com.example.javad.tamringuess;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity1 extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        Button btnEasy = (Button) findViewById(R.id.btnEasy);
        Button btnNormal = (Button) findViewById(R.id.btnNormal);
        Button btnHard = (Button) findViewById(R.id.btnHard);

        btnEasy.setOnClickListener(this);
        btnNormal.setOnClickListener(this);
        btnHard.setOnClickListener(this);


    }

    int counter;


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnEasy:
                counter=3;
                Intent intent=new Intent(MainActivity1.this,MainActivity2.class);
                intent.putExtra("counter",counter);
                startActivity(intent);
                Log.i("test_easy","test_easy");
                break;
            case R.id.btnNormal:
                counter=2;
                intent=new Intent(MainActivity1.this,MainActivity2.class);
                intent.putExtra("counter",counter);
                startActivity(intent);
                break;
            case R.id.btnHard:
                counter=1;
                intent=new Intent(MainActivity1.this,MainActivity2.class);
                intent.putExtra("counter",counter);
                startActivity(intent);
                break;
        }



    }

    private void startActivities(Intent intent_name) {
    }
}
