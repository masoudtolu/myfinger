package com.example.javad.tamringuess;

import android.content.Intent;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity2 extends AppCompatActivity {

    int j;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);



        j = getIntent().getIntExtra("counter",0);

        //assert btn_status != null;
        Button btn_status = (Button) findViewById(R.id.btnGeuss);
        btn_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity2.this, "j", Toast.LENGTH_SHORT).show();
                //Toast.makeText(MainActivity2.this, String.valueOf(j), Toast.LENGTH_SHORT).show();

                TextView tv1 = (TextView) findViewById(R.id.tv_status);
                EditText et = (EditText) findViewById(R.id.ed_number);
                Random rd = new Random();
                int random = rd.nextInt(20);
                j--;

                Log.i("j--", String.valueOf(j));
                int input = Integer.parseInt(et.getText().toString());
                Log.i("input", String.valueOf(input));
                Toast.makeText(MainActivity2.this, String.valueOf(random), Toast.LENGTH_SHORT).show();
                Log.d("j", String.valueOf(j));
                if (j==0) {
                    Toast.makeText(MainActivity2.this, "you loose", Toast.LENGTH_SHORT).show();
                    Intent intent2=new Intent(MainActivity2.this,MainActivity1.class);
                    startActivity(intent2);
                }
                if (j > 0 && input == random) {
                    Toast.makeText(MainActivity2.this, "you win", Toast.LENGTH_SHORT).show();
                    tv1.setText("You Won");
                    Intent intent2=new Intent(MainActivity2.this,MainActivity1.class);
                    startActivity(intent2);
                } else if (j > 0 && input < random) {
                    tv1.setText("Enter bigger");
                } else if (j > 0 && input > random) {
                    tv1.setText("Enter Smaller");
                }


            }
        });
    }
}
